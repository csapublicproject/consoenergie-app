# consoenergie-app

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

# consoenergie

Ce projet a pour objectif de suivre la consomation d'energie familiale pour limiter nos rejets de gaz à effet de serre.

## Technos utilisées

* Python
* ArangoDb
* VueJs
* Docker

## Getting started

## Installation

### Python

To use poetry, we need to install it : https://python-poetry.org/docs/ 

curl -sSL https://install.python-poetry.org | python3 -

To init the poetry project, 

poetry init

... and follow the process

This process permits to add "FastApi" as a development dependencies

### FastApi

https://www.younup.fr/blog/fastapi-nouveau-framework-web-api-python

Tout est dans le fichier consoenergie.py

Pour lancer le serveur, il faut taper la cmd suivate

```
uvicorn consoenergie:app --reload
```

Pour faire un appel à l'api ainsi démarrée

http://127.0.0.1:8000/

Ici, le tuto de base de fastApi

https://www.youtube.com/watch?v=alsWaMLEJd8

### Docker/Arango

lancer arangodb :
sudo docker run --rm --name arangodb-instance -e ARANGO_ROOT_PASSWORD=openSesame -p 8529:8529 -v /home/romane/data/arangodb3:/var/lib/arangodb3 -d arangodb 

stopper arangodb :
sudo docker stop arango-instance

Voir les conteneurs utilisés :
sudo docker ps

<!-- propriété CSS qui peut être appliquée à un élément HTML :
    background-color : définit la couleur de fond d'un élément
    color : définit la couleur du texte
    font-family : définit la police de caractères utilisée pour le texte
    font-size : définit la taille de la police de caractères utilisée pour le texte
    margin : définit la marge autour de l'élément
    padding : définit la distance entre la bordure de l'élément et son contenu
    border : définit les propriétés de la bordure de l'élément, telles que la couleur, l'épaisseur et le style
    width : définit la largeur de l'élément
    height : définit la hauteur de l'élément
    display : définit le type d'affichage de l'élément (tel que block, inline, ou inline-block)
    position : définit la position de l'élément par rapport à ses éléments parents ou à la fenêtre d'affichage
    z-index : définit l'ordre d'empilement des éléments positionnés
    opacity : définit l'opacité de l'élément
    transition : définit les effets de transition qui doivent être appliqués à l'élément lorsqu'il subit une modification
    animation : définit les animations qui doivent être appliquées à l'élément-->
  
<!-- Balises :
    <div>: utilisé pour diviser une page web en sections ou en zones.
    <p>: utilisé pour créer un paragraphe.
    <a>: utilisé pour créer un lien hypertexte.
    <img>: utilisé pour afficher une image.
    <ul>: utilisé pour créer une liste à puces.
    <ol>: utilisé pour créer une liste numérotée.
    <li>: utilisé pour créer un élément de liste.
    <h1> à <h6>: utilisé pour créer des titres et sous-titres.
    <table>: utilisé pour créer un tableau.
    <tr>: utilisé pour créer une ligne dans un tableau.
    <td>: utilisé pour créer une cellule dans un tableau.
    <form>: utilisé pour créer un formulaire.
    <input>: utilisé pour créer un champ de saisie de texte ou une case à cocher.
    <button>: utilisé pour créer un bouton. -->

A FAIRE


conso carbone
gitlab : committer commiter mergerequest
