// import './assets/main.css'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index'
import { store, key } from './components/store'


const app = createApp(App)

app.use(router)

console.log("Init the vuejs app with the store : ", store)
app.use(store, key)

app.mount('#app')