import axios from 'axios';
import type { InjectionKey } from 'vue';
import { ref } from 'vue';
import { createStore, useStore as baseUseStore, Store } from 'vuex';

export interface State {
  etat: boolean;
  toRerender : string;
  collections: string[];
  date: string[];
  donnees: Array<string | number>[][];
  color: string[];
  unit: string[];
  data: {
    labels: any[];
    datasets: any[];
  };
  options: any;
}

export const key: InjectionKey<Store<State>> = Symbol();

let rerenderSeq = 1

export const store = createStore<State>({
  state: {
    toRerender: "rerender-1",
    etat: false,
    collections: [],
    date: [],
    donnees: [],
    color: [],
    unit: [],
    data: {
      labels: [] as any[],
      datasets: [] as any[],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          position: 'right',
        },
      },
      scales: {},
    },
  },
  mutations: {
    updateEtat(state:State, variable: boolean) {
      state.etat = variable;
    },
    updateCollections(
      state: State,
      payload: {
        collections: string[];
        donnees: Array<string | number>[][];
        color: string[];
        unit: string[];
      },
    ) {
      state.collections = payload.collections;
      state.donnees = payload.donnees;
      state.color = payload.color;
      state.unit = payload.unit;
    },
    updateDate(state: State, data: string[]) {
      state.date = data;
      state.data.labels = data;
    },
    zeroDataset(){
      store.state.data.datasets=[];
      store.state.options.scales={};
    },
    updateDataset(
      state: State,
      payload: {
        label: string;
        backgroundColor: any;
        borderColor: any;
        color: any;
        datasetData: Array<number | string>;
        y: string;
        unit: any;
        index: number;
      },
    ) {
      const position = ref('');
      if (payload.index === 1) {
        position.value = 'left';
      } else {
        position.value = 'right';
      }
      const dataset = {
        label: payload.label + ' en ' + payload.unit,
        backgroundColor: payload.backgroundColor,
        borderColor: payload.borderColor,
        data: payload.datasetData,
        yAxisID: payload.y,
      };
      const new_option = {
        position: position,
        ticks: {
          color: payload.color,
        },
        grid: {
          display: false,
        },
      };
      state.data.datasets.push(dataset);
      (state.options.scales as Record<string, any>)[payload.y] = new_option;
    },
    incRerender(state : State) {
      rerenderSeq = rerenderSeq + 1
      state.toRerender = "rerender-" + rerenderSeq
    }
  },
  actions: {
    async fetchCollections(context: any) {
      try {
        const response = await axios.get('/api/collections');
        const collections = response.data.collections;
        const promises = collections.map(async (collection: string) => {
          const response = await axios.get('/api/collections/' + collection);
          return response.data;
        });
        const results = await Promise.all(promises);
        const donnees = results.map((result: any) => result.documents);
        const color = results.map((result: any) => result.couleur);
        const unit = results.map((result: any) => result.unité);
        context.commit('updateCollections', {
          collections,
          donnees,
          color,
          unit,
        });
        try {
          const response = await axios.get('/api/date');
          const date = response.data.date;
          context.commit('updateDate', date);
          // console.log(date, collections);
          for (let i = 0; i < collections.length; i++) {
            // console.log('LISTE DES COLLECTIONS : ', i, ',', collections[i], ',', donnees[i]);
            const variable = Array.from({ length: context.state.date.length }, () => 'a');
            for (let j = 0; j < donnees[i].length; j++) {
              if ('date' in donnees[i][j]) {
                const Index = context.state.date.findIndex((item: string) => item === donnees[i][j]['date']);
                if (Index !== -1 && donnees[i][j] !== undefined) {
                  variable[Index] = donnees[i][j]['conso'];
                }
              }
            }
            while (variable.includes('a')) {
              for (let k = 0; k < variable.length; k++) {
                if (variable[k] === 'a') {
                  if (k === 0 && variable[k + 1] !== 'a') {
                    variable[k] = variable[k + 1];
                  } else if (k !== 0 && variable[k - 1] !== 'a') {
                    variable[k] = variable[k - 1];
                  } else if (k !== 0 && variable[k + 1] !== 'a') {
                    variable[k] = variable[k + 1];
                  }
                }
              }
            }
            const index = i % 2;
            const y = 'y' + i;
            if (i===0){
              context.commit ('zeroDataset')
            }
            context.commit('updateDataset', {
              label: collections[i],
              backgroundColor: color[i],
              borderColor: color[i],
              color: color[i],
              datasetData: variable,
              y: y,
              unit: unit[i],
              index: index,
            });

            context.commit("incRerender")
          }
        } catch (error) {
          console.log('ERROR STORE Date: ', error);
        }
      } catch (error) {
        console.log('ERROR STORE Collections: ', error);
      }
    },
    async fetchPopUp(context: any, variable: boolean) {
      context.commit('updateEtat', variable);
    },
  },
});

export function useStore() {
  return baseUseStore(key);
}

export function showPopUp() {
  if (store.state.etat == false) {
    store.dispatch('fetchPopUp', true);
    return true;
  } else {
    store.dispatch('fetchPopUp', false);
    return false;
  }
}


export function fetchData() {
  store.dispatch('fetchCollections');
}

//setInterval(fetchData, 1000);

fetchData()