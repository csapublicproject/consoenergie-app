from fastapi import FastAPI
from fastapi.responses import JSONResponse
from arango import ArangoClient
from datetime import datetime
from pydantic import BaseModel

class Item(BaseModel):
    collection: str
    date: str | None = None
    consommation: int

app = FastAPI() #créer la FastAPI
client = ArangoClient(hosts='http://localhost:8529') #créer le client vers arangodb
sys_db =client.db('_system', username='root', password='openSesame') #se connecte à arangodb
db = client.db('consoenergie', username='root', password='openSesame') #s'associe à la base de donnée consoenergie
collections = db.collections() #récupere la liste des collections existantes
name_co = [] # créer la liste contenant les noms de collections
for collection in collections:
    if collection['system'] == False :
        name_co.append(collection['name'])
    
@app.get("/")
async def root():
    b={'0':'Sommaire', '1': 'Pour voir la liste des collections aller à l adresse : http://localhost:8000/collections','2':'Pour avoir la liste de documents dans une collection ajouter le nom de la collection après l adresse des collections'}
    return b

@app.get("/collections") #affiche les collections 
async def get_collections():
    # print(db.collections())    
    return {"collections": name_co}
   
@app.get("/collections/{collection_name}") #affiche les documents et l'unite de la collection
async def get_collection_documents(collection_name: str):
    # Vérification si la collection existe
    if collection_name not in name_co:
        return {"error": "La collection n'existe pas"}
    # Récupération des documents de la collection
    collection = db.collection(collection_name)
    documents = []
    unite=''
    cursor = collection.all()
    for document in cursor:
        j={}
        for i in document :
            if i == 'conso' or i=='date':
                j[i]= document[i]
            elif i =='unite':
                unite =str(document[i])
            elif i =='color':
                color=str(document[i])
        if j :   
            documents.append(j)

    # Renvoi des documents de la collection
    return {"collection": collection_name, "documents": documents, "unité": unite, 'couleur':color}

@app.get("/date")
async def get_date():
    date = []
    for collection_name in name_co:
        collection = db.collection(collection_name)
        documents = []
        cursor = collection.all()
        for document in cursor:
            j = {}
            for i in document:
                if i == 'conso' or i == 'date':
                    j[i] = document[i]
            documents.append(j)
        for a in documents:
            for element in a:
                if element == 'date' and a[element] not in date:
                    date.append(a[element])
    def date_key(date):
        return datetime.strptime(date, '%Y-%m-%d')

    sorted_dates = sorted(date, key=date_key)
    
    return{'date':sorted_dates}


### AVANT DE REMETTRE LE POST VERFIFIER CE QU'IL REÇOIT

@app.post('/new')
async def ajout_valeur(item : dict):
    print(item)
    a=item['collection'].split('-')
    if len(a)!=1:
        name_collection=a[0]
        unite=a[1]
        color=a[2]
    else :
        name_collection=a[0]
    date=item['date']
    conso=item['consommation']
    print(date,conso)
    if name_collection not in name_co :
        print(1)
        new_coll=db.create_collection(name_collection)
        new_coll=db.collection(name_collection)
        metadata={'color':color}
        color_data=new_coll.insert(metadata)
        metadata={'unite':unite}
        unite_data=new_coll.insert(metadata)
    coll=db.collection(name_collection)
    metadata={'conso':conso,'date':date}
    new_data=coll.insert(metadata)
    return "ok"
